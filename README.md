# Setting up an E-mail Certificate #

## Obtaining the Certificate ##

* On your Mac desktop, open Safari and go to the [InstantSSL site](https://www.instantssl.com/ssl-certificate-products/free-email-certificate.html).
* Click the **Get Now** link.
* Enter your first name and last name.  These can be anything you wish.  You do not need to provide your full or true name.
* Enter your e-mail address.  This should be the e-mail address for which you want the certificate.  It must be a valid e-mail address where you can receive e-mail.
* Select the appropriate country.
* Verify the Key size is "2048 (High Grade)".
* Enter a Revocation Password and write it down somewhere you will not lose it.
* Uncheck "Opt In?" for "Comodo Newsletter".
* Check "I ACCEPT" to agree to the terms.
* Click the **Next>** button.

* InstantSSL / Comodo will send you an email with the next step.  It should arrive shortly after you complete the last step.
* Given the risk of clicking links in emails, verify that the email is correct.  The odds of a fake email for it arriving at the right time is very low.  The email should say it is from Comodo and say "Dear <Your Name>".
* Check the button link that says "Click & Install Comodo Email Certificate".  Hover over it and verify the link begins with "https://secure.instantssl.com/".
* After you verify it, click the button link "Click & Install Comodo Email Certificate".  If Safari is not your default browser, you may want to copy the link and paste it into Safari.
* You should see a page that says "Attempting to collect and install your Free Certificate".  It will save a file, probably named CollectCCC.p7s, into your Downloads folder.
* Open Finder and go to the Downloads folder.
* Either double-click the CollectCCC.p7s file or right click, select Open With, then select Keychain Access.
* Opening the Keychain Access application from the Utilities folder in Applications.
* Once Keychain Access is running, select "My Certificates" from the Category list.  You should see an entry with the e-mail address you used.  Selecting it should show the certificate and it should have a green check mark and say "This certificate is valid".

* Right click the entry that is the e-mail address you used and select "New Identity Preference".
* Enter the e-mail address into the "Location or Email Address" field.  It should match the e-mail shown in the Certificate dropdown.
* Click the Add button.
* Close the Keychain Access application.

## Using the certificate ##

The person whom you wish to communicate will need the public part of your certificate.  The private key remains on your computer.  The easiest way to share the public key of your new certificate is to send an email to that person and sign it.  Apple Mail will automatically see and install public keys sent to it in an e-mail.

* Open Apple Mail.  If it is already open, close and re-start it to ensure it recognizes the new certificate.
* Click the "Compose new message" button.
* Enter the recipient email in the To field.
* Enter a subject in the Subject field.  You can just use "New Email Certificate" if nothing else comes to mind.
* If there is a From field it should be the e-mail address for your certificate.
* Verify the sunburst with the checkmark in the middle is highlighted and not greyed out.  If it is greyed out, then click it and make sure it is highlighted and there is a checkmark and not an X.
* Your intended recipient will also need to send you their certificate in the same manner.  Opening the email should be enough for Apple Mail to recognize and install the certificate.

Once you have each others public keys, you can encrypt and/or sign messages sent to each other.  Encryption is represented by the padlock button on the Subject line.  Signing is represented by the sunburst with the checkmark in the middle.  A greyed out button indicates the message will not be signed / encrypted.  A bold or highlighted button indicates it will sign and/or encrypt the message.

+ ![alt text](./images/EncryptedIcon.png "Encrypted") Encrypted
+ ![alt text](./images/SignedIcon.png "Signed") Signed
+ ![alt text](./images/UnencryptedIcon.png "Unencrypted") Unencrypted
+ ![alt text](./images/UnsignedIcon.png "Unsigned") Unsigned

Encryption scrambles the message so only the intended recipient can unscramble it.  It is scrambled specifically so only the person who knows the private key of the recipient can read it.

Signing a message is a way to prove you sent the message.  It adds a digital signate using your private key.  Anyone with your public key can then verify the message was signed using your private key.

## Backing up the certificate ##

* Open the Keychain Access application from the Utilities folder in Applications.
* Select the certificate.
* Right click and select "Export".
* Select a location to save the file and click **Save**.
* When prompted, choose / generate a password for the certificate and enter it into the Password and Verify fields.  Write down and/or save this password as you will need it to use this file later.
* Click **OK**.
* You will be prompted to enter your login password to export the key, enter you password and click **Allow**.
* You should now have a file with the extension ".p12" in the location you specified.  You can use this file and the certificate password you chose above to install this certificate on your other devices.  It can also be used as a backup if your device needs to be re-installed.

## Installing the certificate on iOS devices ##

In order to read and send encrypted mail on your iOS device(s), you need to install the certificate on each device.  You will need the certificate and password you created in the section above titled *Backing up the certificate*.

You can use AirDrop to get the certificate from your desktop to your iOS device.  AirDrop uses Wi-Fi and encryption to easily and securely transfer files between Apple devices.  If you would like to read more about it, read [Apple's article on AirDrop](https://support.apple.com/en-us/HT204144)

* On the iOS device, swipe up from the bottom to show the Control Center.
* If the AirDrop section near the bottom left does not say *Contacts Only* or *Everyone*, then tap it and select *Contacts Only*.  If you do not have yourself in your contacts, then you may need to set it to *Everyone*.
* Dismiss Control Center.  Swipe down from the arrow near the top of Control Center or click above Control Center and near the top of the screen.
* On your Mac computer, above a Finder window and select AirDrop on the left side under Favorites.  You can also find it by selecting Go from the menu and then AirDrop.
* Make sure your iOS device is powered on and unlocked.
* Wait a few seconds and Finder on your Mac should show an icon for your iOS device.
* Open another Finder window and find the p12 certificate file you created above.
* Drag the p12 certificate and drop it on the icon for your iOS device in the Finder window with AirDrop.
* Your iOS device should recieve the file and open to a new window titled *Install Profile*.
* Click **Install**.
* Enter the passcode for your device when prompted and click **Done**.
* Click **Install**.
* Click the **Install** action alert at the bottom of the screen.
* Enter the certificate password from above and click **Next**.
* Click **Done**.

* Click the Home button on the iOS device.
* Open Settings.
* Click the section *Mail, Contact, Calendars*.
* Select the email account for the email address entered above.
* Click the **Account** section.
* Click **Advanced**.
* At the bottom click **S/MIME** so it is enabled.
* Click Sign.
* On the next screen, click **Sign** so it is enabled.
* Verify that in the section below, the certificate with the correct email address is checked.  If not click the proper email certificate so it is checked.
* Click the **Advanced** nav button at the top to go back one screen.
* Click the **Encrypt by Default** item.
* Click **Encrypt by default** so it is enabled.
* Verify that in the section below, the certificate with the correct email address is checked.  If not click the proper email certificate so it is checked.
* Click the **Advanced** nav button at the top to go back one screen.
* Click the **Account** nav button at the top to go back one screen.
* Click **Done**.






